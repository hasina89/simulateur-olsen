<?php

 if ( is_page() && !is_front_page() || is_single() || is_category() || is_archive() ) {
 echo '<ul class="fil_ariane">';
 echo '<li><a title="Accueil" rel="nofollow" href="' . esc_url( home_url( '/' ) ). '">Accueil</a></li>';

     if (is_page()) {
         $ancestors = get_post_ancestors($post);

         if ($ancestors) {
         $ancestors = array_reverse($ancestors);

             foreach ($ancestors as $crumb) {
                echo '<li class="active"><a href="'.get_permalink($crumb).'">'.get_the_title($crumb).'</a></li>';
             }
         }
     }

     if (is_single()) {
        $category = get_the_category();
        echo '<li class="active"><a href="'.get_category_link($category[0]->cat_ID).'">'.$category[0]->cat_name.'</a></li>';
     }

     if (is_category()) {
         $category = get_the_category();
         echo '<li class="active">'.$category[0]->cat_name.'</li>';
     }

     // Current page
     if (is_page() || is_single()) {
     echo '<li class="active"><span>'.ad_truncate( $post->post_title , 25 ). '</span></li>';
     }

     if ( is_archive() ) {
        echo '<li class="active"><span>' . post_type_archive_title ( '', false ) . '</span></li>';
     }

    echo '</ul>';

 } elseif (is_front_page()) {
     // Front page
     echo '<ul class="fil_ariane">';
     echo '<li><a title="Accueil" rel="nofollow" href="' . esc_url( home_url( '/' ) ). '">Accueil</a></li>';
     echo '</ul>';
 }
?>
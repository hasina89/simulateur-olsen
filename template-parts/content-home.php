<?php 

global $post;
$post_id = $post->ID;

$all_post_type = array(
    'post',
    'actualite',
    'enseignement',
    'citation',
    'musique',
    'parole',
    'temoignage',
    'pensee',
    'actualite',
    'lifestyle',
    'homme'
);
    // Actu récent
$ad_recent_featered_args = array (
    'posts_per_page'    => 1,
    'post_type'         => $all_post_type,
    'order'             =>'DESC',
    'category'          => false
);
// Actu récent
$ad_recent_args = array (
    'posts_per_page'    => 4,
    'offset'            => 1,
    'post_type'         => $all_post_type,
    'order'             =>'DESC',
    'category'          => false
);

/************/
// Actu plus commenté
$ad_commented_featered_args = array (
    'posts_per_page'    => 1,
    'orderby'          => 'comment_count',
    'post_type'         => $all_post_type,
    'category'          => false    
);
// Actu plus commenté
$ad_commented_args = array (
    'posts_per_page'    => 4,
    'offset'            => 1,
    'orderby'          => 'comment_count',
    'post_type'         => $all_post_type,
    'category'          => false
);

/************/
// Actu plus populaire
$ad_popular_featered_args = array (
    'posts_per_page'    => 1,
    'meta_key'          =>'post_views_count',
    'orderby'           =>'meta_value_num',
    'post_type'         => $all_post_type,
    'order'             =>'DESC',
    'range'             => 'daily',
);
// Actu plus populaire
$ad_popular_args = array (
    'posts_per_page'    => 4,
    'offset'            => 1,
    'range'             => 'daily',
    'freshness'         => false,
    'meta_key'          =>'post_views_count',
    'orderby'           =>'meta_value_num',
    'post_type'         => $all_post_type,
    'order'             =>'DESC'
);

/************/

// post Enseignement feat
$ad_post_right_featered_args = array (
    'posts_per_page'    => 1,
    'post_type'         => 'enseignement',
    // 'category_name'     => 'Enseignements',
    'order'             => 'DESC'
);
// post Enseignement
$ad_post_right_args = array (
    'posts_per_page'    => 5,
    'post_type'         => 'enseignement',
    // 'category_name'     => 'Enseignements',
    'offset'            => 1,
    'order'          => 'DESC'
);

// post lifestyle featured
$ad_lifestyle_featured_args = array (
    'posts_per_page'    => 1,
    'post_type'         => 'lifestyle',
    'order'          => 'DESC'
);

// post lifestyle
$ad_lifestyle_args = array (
    'posts_per_page'    => 5,
    'post_type'         => 'lifestyle',
    'offset'            => 1,
    'order'          => 'DESC'
);

?>

 
<div class="left_content col-sm-8">
    <div class="inner_right_content row">
        <div class="col-xs-12 big_title">
            <h1><span>Explorez</span> <span class="subtitle">les nouveautés</span></h1>                             
        </div>
        <div class="col-xs-12 top_filter">
            <div class="row">
                <!-- Nav tabs -->
                <div class="col-xs-12 top_nav_tab">
                    <div class="row">
                        <div class="col-xs-2 box_txt_filter">
                            <span class="txt_filter">Filtrez : </span>
                        </div>
                        <div class="col-xs-9 all_tab_title">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#recent" aria-controls="recent" role="tab" data-toggle="tab">Récents</a></li>                                
                                <li role="presentation"><a href="#comment" aria-controls="comment" role="tab" data-toggle="tab">Commentés</a></li>
                                <li role="presentation"><a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">Populaires</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-1 last_tab">
                            <img src="<?php echo get_template_directory_uri () ?>/img/menu_tab.png" alt="">
                        </div>
                    </div>
                </div>

                <!-- Tab panes -->
                <div class="col-xs-12 content_nav_tab">
                    <div class="tab-content row">
                        <div role="tabpanel" class="tab-pane active col-xs-12" id="recent">
                            <div class="row">
                                <div class="col-sm-6 featered_menu">
                                    <?php $Q_recent_actu = new WP_Query( $ad_recent_featered_args ); ?>             
                                        <?php if ( $Q_recent_actu->have_posts() ) : while ( $Q_recent_actu->have_posts() ) : $Q_recent_actu->the_post(); ?>

                                            <?php 
                                                // Category 
                                                $category = get_the_category(); 
                                                $category_id = get_cat_ID( $category[0]->cat_name );
                                                $category_link = get_category_link( $category_id );
                                             ?>
                                        <div class="inner_featered_menu">
                                            <div class="img_featered_tab">
                                                <?php if(has_post_thumbnail()) : ?>
                                                    <?php
                                                        $url_img = get_the_post_thumbnail_url();                  
                                                        $fake_url = site_url().'/wp-includes/images/';
                                                        if (stristr($url_img, $fake_url)) {
                                                            $ph_img = '';
                                                        }else {
                                                            $ph_img = $url_img;
                                                        }
                                                    ?>
                                                    <?php if(!empty($ph_img)) : ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                    <?php else: ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                <?php endif; ?>
                                                <div class="cat_featered_tab">
                                                    <a href="<?php echo $category_link; ?>" class="btn btn-primary"><?php echo $category[0]->cat_name; ?></a>
                                                </div>
                                            </div>
                                            <div class="txt_featered_tab">
                                                <div class="sub_txt_featered">
                                                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                    <p class="no_marg">Par <?php the_author() ?>, Il y a <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ''; ?> <span class="total_comment"> <?php comments_number( '0', '1', '%' ); ?></span></p>
                                                </div>
                                                <div class="excerpt_tab">
                                                    <?php the_excerpt(); ?>
                                                </div>
                                                <div class="btn_box_excerpt">
                                                    <a href="<?php the_permalink(); ?>" class="link_more">Lire plus</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); endif; ?> 
                                </div>
                                <div class="col-sm-6 all_cat_dropdown">
                                    <?php $Q_recent_all = new WP_Query( $ad_recent_args ); ?>               
                                    <?php if ( $Q_recent_all->have_posts() ) : while ( $Q_recent_all->have_posts() ) : $Q_recent_all->the_post(); ?>

                                        <?php 
                                            // Category 
                                            $category = get_the_category(); 
                                            $category_id = get_cat_ID( $category[0]->cat_name );
                                            $category_link = get_category_link( $category_id );
                                         ?>
                                        <div class="inner_all_cat_dropdown row">
                                            <div class="col-xs-5 col-md-4 left_img">
                                                <?php if(has_post_thumbnail()) : ?>
                                                    <?php
                                                        $url_img = get_the_post_thumbnail_url();                  
                                                        $fake_url = site_url().'/wp-includes/images/';
                                                        if (stristr($url_img, $fake_url)) {
                                                            $ph_img = '';
                                                        }else {
                                                            $ph_img = $url_img;
                                                        }
                                                    ?>
                                                    <?php if(!empty($ph_img)) : ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                    <?php else: ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-xs-7 col-md-8 right_text_img">
                                                <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                                <p class="date_recent_post">Il y a <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ''; ?></p>
                                                <span class="cat_submenu"><a href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a></span><span class="total_comment"><?php comments_number( '0', '1', '%' ); ?></span>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); endif; ?> 

                                    
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane col-xs-12" id="comment">
                            <div class="row">
                                <div class="col-sm-6 featered_menu">
                                    <?php $Q_comment_actu = new WP_Query($ad_commented_featered_args); ?>  <!--  -->          
                                        <?php if ( $Q_comment_actu->have_posts() ) : while ( $Q_comment_actu->have_posts() ) : $Q_comment_actu->the_post(); ?>

                                            <?php 
                                                // Category 
                                                $category = get_the_category(); 
                                                $category_id = get_cat_ID( $category[0]->cat_name );
                                                $category_link = get_category_link( $category_id );
                                             ?>
                                        <div class="inner_featered_menu">
                                            <div class="img_featered_tab">
                                                <?php if(has_post_thumbnail()) : ?>
                                                    <?php
                                                        $url_img = get_the_post_thumbnail_url();                  
                                                        $fake_url = site_url().'/wp-includes/images/';
                                                        if (stristr($url_img, $fake_url)) {
                                                            $ph_img = '';
                                                        }else {
                                                            $ph_img = $url_img;
                                                        }
                                                    ?>
                                                    <?php if(!empty($ph_img)) : ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                    <?php else: ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                <?php endif; ?>
                                                <div class="cat_featered_tab">
                                                    <a href="<?php echo $category_link; ?>" class="btn btn-primary"><?php echo $category[0]->cat_name; ?></a>
                                                </div>
                                            </div>
                                            <div class="txt_featered_tab">
                                                <div class="sub_txt_featered">
                                                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                    <p class="no_marg">Par <?php the_author() ?> le  <?php $post_date = get_the_date( 'j M, Y' ); echo $post_date; ?> <span class="total_comment"> <?php comments_number( '0', '1', '%' ); ?></span></p>
                                                </div>
                                                <div class="excerpt_tab">
                                                    <?php the_excerpt(); ?>
                                                </div>
                                                <div class="btn_box_excerpt">
                                                    <a href="<?php the_permalink(); ?>" class="link_more">Lire plus</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); endif; ?> 
                                </div>
                                <div class="col-sm-6 all_cat_dropdown">
                                    <?php $Q_comment_all = new WP_Query( $ad_commented_args ); ?>               
                                    <?php if ( $Q_comment_all->have_posts() ) : while ( $Q_comment_all->have_posts() ) : $Q_comment_all->the_post(); ?>

                                        <?php 
                                            // Category 
                                            $category = get_the_category(); 
                                            $category_id = get_cat_ID( $category[0]->cat_name );
                                            $category_link = get_category_link( $category_id );
                                         ?>
                                        <div class="inner_all_cat_dropdown row">
                                            <div class="col-xs-5 col-md-4 left_img">
                                                <?php if(has_post_thumbnail()) : ?>
                                                    <?php
                                                        $url_img = get_the_post_thumbnail_url();                  
                                                        $fake_url = site_url().'/wp-includes/images/';
                                                        if (stristr($url_img, $fake_url)) {
                                                            $ph_img = '';
                                                        }else {
                                                            $ph_img = $url_img;
                                                        }
                                                    ?>
                                                    <?php if(!empty($ph_img)) : ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                    <?php else: ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-xs-7 col-md-8 right_text_img">
                                                <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                                <span class="cat_submenu"><a href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a></span><span class="total_comment"><?php comments_number( '0', '1', '%' ); ?></span>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); endif; ?>                                             
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane col-xs-12" id="popular">
                            <div class="row">
                                <div class="col-sm-6 featered_menu">
                                    <?php $Q_popular_feat_actu = new WP_Query( $ad_popular_featered_args ); ?>              
                                        <?php if ( $Q_popular_feat_actu->have_posts() ) : while ( $Q_popular_feat_actu->have_posts() ) : $Q_popular_feat_actu->the_post(); ?>

                                            <?php 
                                                // Category 
                                                $category = get_the_category(); 
                                                $category_id = get_cat_ID( $category[0]->cat_name );
                                                $category_link = get_category_link( $category_id );
                                             ?>
                                        <div class="inner_featered_menu">
                                            <div class="img_featered_tab">
                                                <?php if(has_post_thumbnail()) : ?>
                                                    <?php
                                                        $url_img = get_the_post_thumbnail_url();                  
                                                        $fake_url = site_url().'/wp-includes/images/';
                                                        if (stristr($url_img, $fake_url)) {
                                                            $ph_img = '';
                                                        }else {
                                                            $ph_img = $url_img;
                                                        }
                                                    ?>
                                                    <?php if(!empty($ph_img)) : ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                    <?php else: ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                <?php endif; ?>
                                                <div class="cat_featered_tab">
                                                    <a href="<?php echo $category_link; ?>" class="btn btn-primary"><?php echo $category[0]->cat_name; ?></a>
                                                </div>
                                            </div>
                                            <div class="txt_featered_tab">
                                                <div class="sub_txt_featered">
                                                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                    <p class="no_marg">Par <?php the_author() ?> le  <?php $post_date = get_the_date( 'j M, Y' ); echo $post_date; ?> <span class="total_comment"> <?php comments_number( '0', '1', '%' ); ?></span></p>
                                                </div>
                                                <div class="excerpt_tab">
                                                    <?php the_excerpt(); ?>
                                                </div>
                                                <div class="btn_box_excerpt">
                                                    <a href="<?php the_permalink(); ?>" class="link_more">Lire plus</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); endif; ?> 
                                </div>
                                <div class="col-sm-6 all_cat_dropdown">
                                    <?php $Q_popular_all = new WP_Query( $ad_popular_args ); ?>             
                                    <?php if ( $Q_popular_all->have_posts() ) : while ( $Q_popular_all->have_posts() ) : $Q_popular_all->the_post(); ?>

                                        <?php 
                                            // Category 
                                            $category = get_the_category(); 
                                            $category_id = get_cat_ID( $category[0]->cat_name );
                                            $category_link = get_category_link( $category_id );
                                         ?>
                                        <div class="inner_all_cat_dropdown row">
                                            <div class="col-xs-5 col-md-4 left_img">
                                                <?php if(has_post_thumbnail()) : ?>
                                                    <?php
                                                        $url_img = get_the_post_thumbnail_url();                  
                                                        $fake_url = site_url().'/wp-includes/images/';
                                                        if (stristr($url_img, $fake_url)) {
                                                            $ph_img = '';
                                                        }else {
                                                            $ph_img = $url_img;
                                                        }
                                                    ?>
                                                    <?php if(!empty($ph_img)) : ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                    <?php else: ?>
                                                        <a href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-xs-7 col-md-8 right_text_img">
                                                <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                                <span class="cat_submenu"><a href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a></span><span class="total_comment"><?php comments_number( '0', '1', '%' ); ?></span>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); endif; ?>                                             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

            </div>
        </div>
        <div class="col-xs-12 playlist_home">
            <a href="<?php echo site_url('playlist-musique'); ?>"><img src="<?php echo get_template_directory_uri () ?>/img/playlist_home.png" alt="" class="img-responsive"></a>
        </div>
        <div class="col-xs-12 more_cat_home_featered">
            <div class="row">
                <div class="col-sm-6 col-xs-12 left_cat_home_featered">
                    <h3 class="subtitle_secondary"><a href="<?php echo site_url('lifestyle') ?>">Lifestyle</a><small>Familles, couple, ...</small></h3>
                    <div class="top_more_cat_home_featered">
                        <?php $Q_lifestyle_feat = new WP_Query( $ad_lifestyle_featured_args ); ?>               
                        <?php if ( $Q_lifestyle_feat->have_posts() ) : while ( $Q_lifestyle_feat->have_posts() ) : $Q_lifestyle_feat->the_post(); ?>

                            <?php 
                                // Category 
                                $category = get_the_category(); 
                                $category_id = get_cat_ID( $category[0]->cat_name );
                                $category_link = get_category_link( $category_id );
                                $format = get_post_format( $post->ID );  
                                if (!empty($format)) {
                                    $format = get_post_format( $post->ID ); 
                                }else {
                                    $format = 'quote';
                                }
                             ?>
                        <div class="top_more_img_featered">
                            <div class="inner_img_all_cat_more ">
                                <?php if(has_post_thumbnail()) : ?>
                                    <?php
                                        $url_img = get_the_post_thumbnail_url();
                                        $fake_url = site_url().'/wp-includes/images/';
                                        if (stristr($url_img, $fake_url)) {
                                            $ph_img = '';
                                        }else {
                                            $ph_img = $url_img;
                                        }
                                    ?>
                                    <?php if(!empty($ph_img)) : ?>
                                        <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                    <?php else: ?>
                                        <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <a class="<?php echo 'at_'.$format; ?>" href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <div class="top_more_txt_featered">
                            <div class="top_more_sub_txt_featered">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <p class="no_marg">Par <?php the_author() ?> le  <?php $post_date = get_the_date( 'j M, Y' ); echo $post_date; ?> <span class="total_comment"> <?php comments_number( '0', '1', '%' ); ?></span></p>
                            </div>
                            <div class="top_more_excerpt">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="btn_box_excerpt">
                                <a href="<?php the_permalink(); ?>" class="link_more">Lire plus</a>
                            </div>
                        </div> 
                        <?php endwhile; wp_reset_query(); endif; ?>  
                    </div>
                    <!-- top_more_list -->
                    <?php $Q_lifestyle = new WP_Query( $ad_lifestyle_args ); ?>               
                    <?php if ( $Q_lifestyle->have_posts() ) : while ( $Q_lifestyle->have_posts() ) : $Q_lifestyle->the_post(); ?>

                        <?php 
                            // Category 
                            $category = get_the_category(); 
                            $category_id = get_cat_ID( $category[0]->cat_name );
                            $category_link = get_category_link( $category_id );
                            $format = get_post_format( $post->ID );  
                            if (!empty($format)) {
                                $format = get_post_format( $post->ID ); 
                            }else {
                                $format = 'quote';
                            }
                         ?>
                        <div class="item_all_cat_more row no_marg">
                            <div class="col-xs-12 no_padd">
                                <div class="row">
                                    <div class="col-xs-5 col-md-4 img_all_cat_more">
                                        <div class="inner_img_all_cat_more ">
                                            <?php if(has_post_thumbnail()) : ?>
                                                <?php
                                                    $url_img = get_the_post_thumbnail_url();
                                                    $fake_url = site_url().'/wp-includes/images/';
                                                    if (stristr($url_img, $fake_url)) {
                                                        $ph_img = '';
                                                    }else {
                                                        $ph_img = $url_img;
                                                    }
                                                ?>
                                                <?php if(!empty($ph_img)) : ?>
                                                    <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                <?php else: ?>
                                                    <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                <?php endif; ?>
                                            <?php else : ?>
                                                <a class="<?php echo 'at_'.$format; ?>" href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-7 col-md-8 txt_all_cat_more">
                                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                        <span class="cat_submenu"><a href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a></span><span class="total_comment"><?php comments_number( '0', '1', '%' ); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_query(); endif; ?> 

                    
                </div>
                <div class="col-sm-6 col-xs-12 right_cat_home_featered">
                    <h3 class="subtitle_secondary"><a href="<?php echo site_url('enseignement') ?>">Enseignements</a><small>Foi, Finance, ...</small></h3>
                    <div class="top_more_cat_home_featered">
                        <?php $Q_post_right_feat = new WP_Query( $ad_post_right_featered_args ); ?>             
                        <?php if ( $Q_post_right_feat->have_posts() ) : while ( $Q_post_right_feat->have_posts() ) : $Q_post_right_feat->the_post(); ?>

                            <?php 
                                // Category 
                                $category = get_the_category(); 
                                $category_id = get_cat_ID( $category[0]->cat_name );
                                $category_link = get_category_link( $category_id );
                                // Post format 
                                $format = get_post_format( $post->ID );  
                                if (!empty($format)) {
                                    $format = get_post_format( $post->ID ); 
                                }else {
                                    $format = 'quote';
                                }
                             ?>
                        <div class="top_more_img_featered">    
                            <div class="inner_img_all_cat_more ">
                                <?php if(has_post_thumbnail()) : ?>
                                    <?php
                                        $thumb_popular_f_id = get_post_thumbnail_id();
                                        $thumb_popular_f = wp_get_attachment_image_src($thumb_popular_f_id,'carousel', true);
                                        $fake_url = site_url().'/wp-includes/images/';
                                        if (stristr($thumb_popular_f[0], $fake_url)) {
                                            $ph_img = '';
                                        }else {
                                            $ph_img = $thumb_popular_f[0];
                                        }
                                    ?>
                                    <?php if(!empty($ph_img)) : ?>
                                        <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                    <?php else: ?>
                                        <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <a class="<?php echo 'at_'.$format; ?>" href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <div class="top_more_txt_featered">
                            <div class="top_more_sub_txt_featered">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <p class="no_marg">Par <?php the_author() ?> le  <?php $post_date = get_the_date( 'j M, Y' ); echo $post_date; ?> <span class="total_comment"> <?php comments_number( '0', '1', '%' ); ?></span></p>
                            </div>
                            <div class="top_more_excerpt">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="btn_box_excerpt">
                                <a href="<?php the_permalink(); ?>" class="link_more">Lire plus</a>
                            </div>
                        </div> 
                        <?php endwhile; wp_reset_query(); endif; ?>  
                    </div>
                    <!-- top_more_list -->
                    <?php $Q_post_right_all = new WP_Query( $ad_post_right_args ); ?>
                    <?php if ( $Q_post_right_all->have_posts() ) : while ( $Q_post_right_all->have_posts() ) : $Q_post_right_all->the_post(); ?>

                        <?php 
                            // Category 
                            $category = get_the_category(); 
                            $category_id = get_cat_ID( $category[0]->cat_name );
                            $category_link = get_category_link( $category_id );
                            // Post format 
                            $format = get_post_format( $post->ID );  
                            if (!empty($format)) {
                                $format = get_post_format( $post->ID ); 
                            }else {
                                $format = 'quote';
                            }
                         ?>
                        <div class="item_all_cat_more row no_marg">
                            <div class="col-xs-12 no_padd">
                                <div class="row">
                                    <div class="col-xs-5 col-md-4 img_all_cat_more">
                                        <div class="inner_img_all_cat_more ">
                                            <?php if(has_post_thumbnail()) : ?>
                                                <?php
                                                    $thumb_popular_f_id = get_post_thumbnail_id();
                                                    $thumb_popular_f = wp_get_attachment_image_src($thumb_popular_f_id,'carousel', true);
                                                    $fake_url = site_url().'/wp-includes/images/';
                                                    if (stristr($thumb_popular_f[0], $fake_url)) {
                                                        $ph_img = '';
                                                    }else {
                                                        $ph_img = $thumb_popular_f[0];
                                                    }
                                                ?>
                                                <?php if(!empty($ph_img)) : ?>
                                                    <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                <?php else: ?>
                                                    <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" alt="" ></a>
                                                <?php endif; ?>
                                            <?php else : ?>
                                                <a class="<?php echo 'at_'.$format; ?>" href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-7 col-md-8 txt_all_cat_more">
                                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                        <span class="cat_submenu"><a href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a></span><span class="total_comment"><?php comments_number( '0', '1', '%' ); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_query(); endif; ?>  
                </div>
            </div>
        </div>
    </div>
</div>

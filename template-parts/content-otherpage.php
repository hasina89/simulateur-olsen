<!-- Main -->
                <div id="main" class="wrapper style1">
                    <div class="container">
                        <header class="major">
                            <h2><?php the_title(); ?></h2>
                            <p>
                                <?php if ( has_excerpt() ) : ?>
                                    <?php the_excerpt(); ?>
                                <?php endif; ?>
                            </p>
                        </header>

                        <!-- Content -->
                            <section id="content">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <div class="picture_full">                                    
                                        <?php the_post_thumbnail('big_image',array('class'=>'img-responsive')); ?>
                                    </div>
                                <?php endif; ?>
                                
                                <?php the_content(); ?>
                            </section>

                    </div>
                </div>
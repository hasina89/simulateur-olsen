<?php

/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage somaco
 */
?>

<?php if (is_front_page()) : ?>
    <?php get_template_part('template-parts/content', 'home'); ?>
<?php elseif ($post->ID == FOLLOW_PAGE_ID) : ?>
    <?php get_template_part('template-parts/content', 'follow'); ?>
<?php elseif ($post->ID == NOUS_REJOINDRE_PAGE_ID) : ?>
    <?php get_template_part('template-parts/content', 'join'); ?> 
<?php elseif ($post->ID == NOUS_CONTACTER_PAGE_ID) : ?>
    <?php get_template_part('template-parts/content', 'contact'); ?>
<?php elseif ($post->ID == NEWSLETTER_PAGE_ID) : ?>
    <?php get_template_part('template-parts/content', 'newsletter'); ?>
<?php else : ?>
    <?php get_template_part('template-parts/content', 'otherpage'); ?>
<?php endif; ?>
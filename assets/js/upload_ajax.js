
jQuery(function($){

    $('#submit_photo').click(function (e) {
        e.preventDefault();

        var vis = $(this);
        var forme = $('#form_photo'); 
        var Fserialize = forme.serialize();
        console.log(Fserialize);

        var fileData = new FormData();
        var file = forme.find('input[type="file"]');
        var individual_file = file[0].files[0];
        fileData.append("file", individual_file);
        fileData.append("action", 'upload_ajax');        
        fileData.append("fData", Fserialize);        

        var txtButton = vis.text();
        var container = $('#result_photo');

        $.ajax({
            url: ajaxurl,      
            data: fileData,
            type:"POST",
            contentType: false,
            processData: false,
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
            },
            success:function(data){
                // console.log(data);
                vis.text(txtButton);
                var stringData;
                stringData = $.parseJSON(data);
                container.empty();
                if(stringData.code == 1){
                    container.html('<p class="successMsg">Photo enregistrée !</p>');
                }else {
                    container.html('<p class="errorMsg">'+ stringData.msg +'</p>');                    
                }
            }
        });
        return false;
    });
});

jQuery(function($){
    // For Step 13
	$('#confirm_pool').click(function (e) {
        var vis = $(this);
        var txtButton = vis.text();
        var container = $('#response');
        var forme = $('#form_send_mail');   
        $.ajax(ajaxurl, {     
            data: { 
                'action' : 'pool', 
                'data' : forme.serialize()             
            },            
            type:"POST",
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
                container.empty();
            },
            success:function(data){
                // console.log(data);
            	vis.text(txtButton);
            	var stringData;
            	stringData = $.parseJSON(data);
                container.empty();
                container.html(stringData.msg); 
            }
        });
        return false;
    });

    // For each Simulation in the Lead list
    $('.btn_pdf_export').click(function (e) {
        var vis = $(this);
        var txtButton = vis.html();
        var forme = $('.form_pdf');
        var boxPdf = forme.find('#box_pdf');
        // console.log(forme.serialize() );

        $.ajax(ajaxurl, {     
            data: { 
                'action' : 'pool', 
                'data' : forme.serialize()             
            },            
            type:"POST",
            beforeSend:function(){
                vis.html('Veuillez patienter ...');
                boxPdf.html('');
            },
            success:function(response){
                // console.log(response);
                vis.html(txtButton);

                if(response){
                    boxPdf.html('<a download href="'+response+'" class="attach_pdf button button-primary">Télécharger</a>');
                }else {
                    boxPdf.html("<span>Une erreur s'est produite, Veuillez raffraichir la page et réessayer SVP</span>");
                }

            }
        });
        return false;
    });

    if ( $('.tablesorter').length ){
        var href = $('.export-csv-leads').attr('href');
        $('.tablesorter')
            .tablesorter()
            .bind('sortEnd', function(e, table) {
               if ( $('.sortable-province').attr('aria-sort') == "ascending" ){            
                    $('.export-csv-leads').attr('href', href + '&orderby=province&order=asc');
                }else if( $('.sortable-province').attr('aria-sort') == "descending" ){
                    $('.export-csv-leads').attr('href', href + '&orderby=province&order=desc');
                }
                else if ( $('.sortable-prix').attr('aria-sort') == "ascending" ){            
                    $('.export-csv-leads').attr('href', href + '&orderby=prix&order=asc');
                }else if( $('.sortable-prix').attr('aria-sort') == "descending" ){
                    $('.export-csv-leads').attr('href', href + '&orderby=prix&order=desc');
                }
            });
    }

    if ( $('#tri-province').length ){
        var baseHref = $('#export-result').attr('href');
        var saveHref = baseHref;
        $('#tri-province').click(function(){
            var $this = $(this);
            var province = $('#tri-province-select').val();
            var txtButton = $(this).html();
            
            
            $.ajax(ajaxurl, {
                data: {
                    'action': 'tri_par_province',
                    'province' : province
                },
                type: 'POST',
                beforeSend: function(){
                    $this.html('Veuillez patienter...');
                },
                success: function(resp){
                    $this.html(txtButton);
                    if (resp == "empty"){
                        alert('Aucun province choisi !');
                    }else{
                        var stringResp = $.parseJSON(resp);
                        $('tbody#the-list').html(stringResp.html);
                        $('#total-result').text(stringResp.total);
                        
                        if ( stringResp.total > 0){
                            $('#export-result')
                                .fadeIn()
                                .attr('href', saveHref + '&province='+stringResp.province);
                        }else{
                            $('#export-result').fadeOut();
                        }

                    }

                }
            });
            return false;
        });
    }

});

jQuery(function($){
	$('#confirm_pool').click(function (e) {
        var vis = $(this);
        var txtButton = vis.text();
        var container = $('#response');
        var forme = $('#form_send_mail');   
        $.ajax(ajaxurl, {     
            data: { 
                'action' : 'pool', 
                'data' : forme.serialize()             
            },            
            type:"POST",
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
            },
            success:function(data){
                // console.log(data);
            	vis.text(txtButton);
            	var stringData;
            	stringData = $.parseJSON(data);
                container.empty();
                container.html(stringData.msg); 
            }
        });
        return false;
    });
});
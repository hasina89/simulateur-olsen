<?php get_header(); ?>
<main class="pageContent">
    <div class="pageLeft">
        <div class="breadcrumb">
            <a href="<?= site_url(); ?>" title="Finaritra">Finaritra</a> | 
            <a class="active" title="<?= the_title(); ?>"><?= the_title(); ?></a>
        </div>
        
        <?php while ( have_posts() ) : the_post(); ?>
            <h1 class="titrePage"><?= the_title(); ?></h1>
            <h2 class="sousTitre">AKI.Radio</h2>

            <?php 
                $img_post = get_the_post_thumbnail_url(get_the_ID());
                if ($img_post) :  ?>
                    <div class="feat_img">
                        <img src="<?= $img_post; ?>" alt="<?= the_title(); ?>"/>
                    </div>
                <?php endif; ?>
            
            <?= the_content(); ?>

        <?php endwhile; wp_reset_query(); ?>

    </div>
    <?php get_template_part( 'template-parts/content', 'right' ); ?> 
</main>
<?php get_footer(); ?>
<?php 
	$step = 2;

	if(!isset($uid)){
        $uid = $current_user_id;
    }

	$data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
	$content_step_array = array();
	update_customer_step($uid, $step, $content_step_array, null, $pool_time);  

	// View
	$view = locate_template( 'pools/step/step'.$step.'.php', false, false );
	include( $view ); 
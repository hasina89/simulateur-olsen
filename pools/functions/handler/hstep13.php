<?php 
	$step = 13;
    
    $data_step = sanitize_content_step(get_item_step_data($cur_simulation_id, 'content_step'));
    $data_step_img = get_item_step_data($cur_simulation_id, 'img');  
    $data_step_pool_time = get_item_step_data($cur_simulation_id, 'pool_time');   


    if (isset($traitement_eau) && !empty($traitement_eau)) {
        $content_step_array      = array( 'traitement_eau' => json_encode($traitement_eau) );
        $data_step->traitement_eau = $traitement_eau;
        $content_step_array      = $data_step;

        update_customer_step($current_user_id, $step, $content_step_array, $data_step_img);  
    }
    elseif($vstep && $uid)
    {
        $data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
        $content_step_array = array(
            'modele'         => $data_step->modele,
            'type_pose'      => $data_step->type_pose,
            'choix_couleur'  => $data_step->choix_couleur,
            'choix_margelle' => $data_step->choix_margelle,

            'eclairage_id'     => $data_step->eclairage_id,
            'eclairage_format' => $data_step->eclairage_format,
            'eclairage_prix'   => $data_step->eclairage_prix,
            'nb_unite'         => $data_step->nb_unite,

            'local_teknik'   => $data_step->local_teknik,
            
            'distance_id'    => $data_step->distance_id,
            'distance_type'  => $data_step->distance_locale,
            'distance_value' => $data_step->distance_value,
            'distance_prix'  => $data_step->distance_prix,
            
            'type_skimmer'   => $data_step->type_skimmer,
            'type_acces_maison'   => $data_step->type_acces_maison,
            'option_supp'   => $data_step->option_supp,
            'traitement_eau'   => $data_step->traitement_eau,
        );       

        if($pool_time){
            update_customer_step($uid, $vstep, $content_step_array, null, $pool_time);
        }else {
            update_customer_step($uid, $vstep, $content_step_array, null );
        }
    }
    else {
        $content_step_array = $data_step;
        if(!isset($uid)){
            $uid = $current_user_id;
        }

        if($cur_step == 3 && $next_step == 13){


            if (isset($type_pose) && !empty($type_pose)) {
                $content_step_array      = array( 'type_pose' => json_encode($type_pose) );
                $data_step->type_pose = $type_pose;
                $content_step_array      = $data_step;                
                update_customer_step($uid, 13, $content_step_array, null);
            }
        }else {

            if($pool_time){
                update_customer_step($uid, 13, $content_step_array, null, $pool_time);
            }else {
                update_customer_step($uid, 13, $content_step_array, null);
            }
        }
    }

    // Get Data
    $modele_id          = $data_step->modele;
    $pose_id            = $data_step->type_pose;
    $couleur_id         = $data_step->choix_couleur;
    $margelle_id        = $data_step->choix_margelle;

    $eclairage_id     = $data_step->eclairage_id;
    $eclairage_format = $data_step->eclairage_format;
    $eclairage_prix   = $data_step->eclairage_prix;
    $nb_unite         = $data_step->nb_unite;

    $local_technique_id = $data_step->local_teknik;
    $distance_locale_id = $data_step->distance_id;
    
    $distance_id        = $data_step->distance_id;
    $distance_type      = $data_step->distance_type;
    $distance_value     = $data_step->distance_value;
    $distance_prix      = $data_step->distance_prix;
    
    $skimmer_id         = $data_step->type_skimmer;
    $acces_maison_id    = $data_step->type_acces_maison;
    $option_supp_ids    = $data_step->option_supp;
    $traitement_eau_id  = $data_step->traitement_eau;

    $data_user = get_user_pool_data($current_user_id);
    $email_user = $data_user[0]->email;
    
    // View
    $view = locate_template( 'pools/step/step'.$step.'.php', false, false );
    include( $view ); 
<?php 
	$step = 3;

	if (isset($modele) && $modele != null) 
	{
    	$content_step_array = array( 'modele' => $modele );
    	update_customer_step($current_user_id, $step, $content_step_array);  
	}
	elseif($vstep && $uid)
	{
		$data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
		$content_step_array = array( 'modele' => $data_step->modele );
    	update_customer_step($uid, $vstep, $content_step_array);
    	$current_user_id = $uid;
	}
    // Get Data
    $modele_id = get_content_step_key($current_user_id, 'modele');

    // Pose IDs
    $pose_ids = allow_tax_ids($modele_id, 'pose');
	
	// View
	$view = locate_template( 'pools/step/step'.$step.'.php', false, false );
	include( $view ); 
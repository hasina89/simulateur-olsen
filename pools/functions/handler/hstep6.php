<?php 
	$step = 6;
    $data_step = sanitize_content_step(get_item_customer_info($current_user_id, 'content_step'));
    
    if (isset($choix_margelle) && $choix_margelle != null) {  
        $content_step_array = array( 'choix_margelle' => $choix_margelle );
        $data_step->choix_margelle = $choix_margelle;
        $content_step_array = $data_step;
        update_customer_step($current_user_id, $step, $content_step_array);  
    }
    elseif($vstep && $uid)
    {
        $data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
        $content_step_array = array(
            'modele'         => $data_step->modele,
            'type_pose'      => $data_step->type_pose,
            'choix_couleur'  => $data_step->choix_couleur,
            'choix_margelle' => $data_step->choix_margelle
        );
        update_customer_step($uid, $vstep, $content_step_array); 
    }

    // Get Data
    $modele_id   = $data_step->modele;
    $pose_id     = $data_step->type_pose;
    $couleur_id  = $data_step->choix_couleur;
    $margelle_id = $data_step->choix_margelle;

    // Eclairage IDs
    $eclairage_ids = allow_tax_ids($modele_id, 'eclairage');
    
    // View
    $view = locate_template( 'pools/step/step'.$step.'.php', false, false );
    include( $view ); 
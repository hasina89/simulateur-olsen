<?php 
$current_url = get_home_url(null, $wp->request, null).'/';
if(!defined('CUR_URL')) define('CUR_URL', $current_url);

// Debug and print
function dd($data, $title = 'Data =>'){
	echo '<pre style="padding: 10px 20px;">';
	echo '<span>'.$title.' => </span>';
	print_r($data);
	echo '</pre>';
}

/* Get Modele Piscine */
function get_pools(){
	$args = array (
	    'posts_per_page'    => -1,
	    'post_type'         => 'pool',
	    'order'             =>'ASC',
	);
	$Q_all = new WP_Query( $args );
	return $Q_all;
}

/* Get Type Pose */
function get_taxs($tax, $pose_a_exclure = array()){
	$args = array(
        'taxonomy'   => $tax,
        'order'      => 'ASC',
        'orderby '   => 'term_id',
        'hide_empty' => false,
        'exclude'    => $pose_a_exclure,
	);
	$the_query = new WP_Term_Query($args);
	return $the_query->get_terms();
}

// Decode data
function sanitize_content_step($content){
    return json_decode($content);
}

// Get content step Key 
function get_content_step_key($current_user_id, $key_name){
	$data_step = sanitize_content_step(get_item_customer_info($current_user_id, 'content_step'));
    $res = $data_step->$key_name;
    return $res;
}

// Count number of key
function count_keys($array_data){
	$count = count(array_keys((array)$array_data));
	return $count;
}

// Count total price
function total_price($array_price){
	$t_price = 0;
	foreach ($array_price as $key => $price) {
		$t_price += $price;
	}
	return $t_price;
}

// Count total price TVAC
function total_price_tvac($total, $tva_percent){
    $tva = ($total * $tva_percent) / 100;
    $tvac = $tva + $total;
    return $tvac;
}

// Get field value
function give_field($field, $id, $tax=false){
	if($tax){
		$res = get_field( $field, $tax.'_'.$id );
	}else {
		$res = get_field($field, $id);
	}
	return $res;
}

// Get Taxonomy value
function get_tax_field($id){
	$res = get_field('numero_modele', $id);
	return $res;
}

// Price
function format_price($prix){
    return number_format($prix, 0, '', ' ');
}

// Array tax
function array_tax($taxs){
    $tax_ids = array();
    if(!empty($taxs)){
    	foreach ($taxs as $tax) {
	    	$tax_ids[] = $tax->term_id;
	    }
    }
    return $tax_ids;
}


// Allowed taxonomy (défault values)
function allow_tax_ids($modele_id, $tax_name){
	$taxs = get_the_terms($modele_id, $tax_name);
    $all_ids = array_tax($taxs);
    return $all_ids;
}

// conditionnal_price
function conditionnal_price($modele_id, $tax_id, $tax, $field_name='prix'){
	$tax_ids = allow_tax_ids($modele_id, $tax);
    if (in_array($tax_id, $tax_ids)){
        $prix_tax = 0;
    }else {
        $prix_tax = get_field( $field_name, $tax.'_'.$tax_id );
    }
    return $prix_tax;
}

// conditionnal_tax_price
function conditionnal_tax_price($modele_id, $tax_id, $tax, $field_name='prix'){
	$tax_ids = allow_tax_ids($modele_id, $tax);

	$prix = get_field( $field_name, $tax.'_'.$tax_id);
	$prix_selon_model  = get_field('prix_selon_model', $tax.'_'.$tax_id);
    $liste_models       = get_field('liste_model', $tax.'_'.$tax_id);

    if (in_array($tax_id, $tax_ids)){
        $prix_tax = 0;
    }else {
    	$prix_tax = $prix;

        if($prix_selon_model == 'oui'){
            foreach ($liste_models as $key => $liste_model) {
                $model_predefini = $liste_model['model'];
                if($model_predefini == $modele_id){
                    $prix_tax = $liste_model['prix_correspondant'];
                } 
            }
        }
    }
    return $prix_tax;
}

// conditionnal_tax_price_pose
function conditionnal_tax_price_pose($modele_id, $tax_id, $tax, $pose_id, $field_name='prix'){
	$tax_ids = allow_tax_ids($modele_id, $tax);

	$prix = get_field( $field_name, $tax.'_'.$tax_id);
	$prix_selon_model  = get_field('prix_selon_model', $tax.'_'.$tax_id);
    $liste_models       = get_field('liste_model', $tax.'_'.$tax_id);

    if (in_array($tax_id, $tax_ids)){
        $prix_tax = 0;
    }else {
    	$prix_tax = $prix;

        if($prix_selon_model == 'oui'){
            foreach ($liste_models as $key => $liste_model) {
                $model_predefini = $liste_model['model'];
                $pose_predefini = $liste_model['pose'];
                if($model_predefini == $modele_id && $pose_predefini == $pose_id){
                    $prix_tax = $liste_model['prix_correspondant'];
                } 
            }
        }
    }
    return $prix_tax;
}

// conditionnal_tax_price
function price_from_pose($pose_id, $tax_id, $tax, $field_name='prix'){
    $tax_ids = allow_tax_ids($pose_id, $tax);

    $prix = get_field( $field_name, $tax.'_'.$tax_id);
    $prix_selon_pose  = get_field('prix_selon_pose', $tax.'_'.$tax_id);
    $liste_poses       = get_field('liste_pose', $tax.'_'.$tax_id);

    /*if (in_array($tax_id, $tax_ids)){
        $prix_tax = 0;
    }else {
        $prix_tax = $prix;

    }*/
    if($prix_selon_pose == 'oui'){
        foreach ($liste_poses as $key => $liste_pose) {
            $pose_predefini = $liste_pose['pose'];
            if($pose_predefini == $pose_id){
                $prix_tax = $liste_pose['prix_correspondant'];
            } 
        }
    }else {
        $prix_tax = $prix;
    }
    return $prix_tax;
}

// Sanitize Price
function sanitize_price($prix, $signe='+'){
    $calc = $prix == 0 ? 'Inclus' : $signe.' '.$prix.'  €';
    return $calc;
}

// Real distance price
function real_distance_price($distance_value, $distance_prix, $seuil=6 ){
    if($distance_value <= $seuil){
        $d_price = 1 * $distance_prix;
    }else {
        $subSueil = $seuil - 1;
        $realDistance = $distance_value - $subSueil;
        $d_price = $realDistance * $distance_prix;
    }
    return $d_price;
}

// Subscribe on Mailchimp
function subscribeMailchimp($email, $firstname, $lastname, $phone=null) {
    $authToken = '2e873622de7d83ad97b332fc9109ee9b-us8';
    $list_id = '6414d38d74';
    // The data to send to the API  
    
    $postData = array(
        "email_address" => $email, 
        "status" => "subscribed", 
        "merge_fields" => array(
            "PHONE" => $phone,
            'FNAME' => $firstname,
            'LNAME' => $lastname
        )
    );

    // Setup cURL
    $dataCenter = substr($authToken,strpos($authToken,'-')+1);

    $ch = curl_init('https://'.$dataCenter.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: apikey '.$authToken,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    // Send the request
    $response = curl_exec($ch);
    $result = json_decode( $response);

    /*if (isset($result->id) && $result->id != null) {
        print_r($result->id);
    }else {
        print_r($result);
    }*/

}
<?php

add_action( 'wp_ajax_code_promo_ajax', 'codepromo_ajax' );
add_action( 'wp_ajax_nopriv_code_promo_ajax', 'codepromo_ajax' );


function codepromo_ajax(){

    if (isset ( $_POST ) ) {
        parse_str($_POST['data'], $Data);
        extract($Data);

        $AdminCodePromo = get_field('code_promo', 'option');
        $AdminCodePromoValidation = get_field('validite_code', 'option');

        $response = array();
        
        if (isset($code_promo) && !empty($code_promo) && $AdminCodePromo == $code_promo ) {
            if(update_customer_code_promo($id_user, $code_promo )){
                $response['status'] = 1;
                $response['msg'] = "Vous avez introduit le code promo <b>".$AdminCodePromo."</b> valable jusqu'au <b>".$AdminCodePromoValidation."</b>, veuillez contacter le +32 476 174 336   afin de connaître votre remise.";
            }
        } else {
            $response['status'] = 0;
            $response['msg'] = 'Pas de Code Promo inséré, contactez-nous au <a href="tel:+32476174336">+32 476 174 336</a> pour connaitre les codes promo du moment !';
        }

        echo json_encode( $response);
        die();
    }
}
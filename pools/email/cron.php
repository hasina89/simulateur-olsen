<p>Bonjour <?= $Nom; ?></p>

<p><?= $subject; ?></p>

<p>Vous avez fait une simulation de piscine sur notre site le <b><?= $Date; ?></b> mais vous n'êtes pas arrivé jusqu'au bout, vous vous êtes arreté à l'étape <b><?= $lastStep; ?></b>.</p>
<p>En quoi pourrions-nous vous aider pour avoir votre Simulation complète ?</p>
<p>Vous pouvez finaliser votre simulation sur le site <a href="<?= $Url_site; ?>" ><?= $Url_site; ?></a> avec vos informations ci-dessous</p>

<h3>Vos informations :</h3>
<table border="1" width="500px">
	<tr>
		<td>Piscine pour :</td>
		<td><?= $PoolTime; ?></td>
	</tr>
	<tr>
		<td>Nom</td>
		<td><?= $Nom; ?></td>
	</tr>
	<tr>
		<td>Prénom</td>
		<td><?= $Prenom; ?></td>
	</tr>	
	<tr>
		<td>Code postal</td>
		<td><?= $Cp; ?></td>
	</tr>
	<tr>
		<td>Téléphone</td>
		<td><?= $Phone; ?></td>
	</tr>
	<tr>
		<th>Email</th>
		<td><?= $Mail; ?></td>
	</tr>
</table>

<p>Cordialement,</p>
<p>L'équipe du Energy & Welness srl</p>
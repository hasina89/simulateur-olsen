<?php get_header(); ?>
    <main class="pageContent">
        <div class="pageLeft">
            <div class="breadcrumb">
                <a href="<?= site_url(); ?>" title="Finaritra">Finaritra</a> | 
                <a class="active" title="<?php echo post_type_archive_title( '', false ) ?>"><?php echo post_type_archive_title( '', false ) ?></a>
            </div>
            <h1 class="titrePage"><?php echo post_type_archive_title( '', false ) ?></h1>
            <h2 class="sousTitre">AKI.Radio</h2>

            <?php if ( have_posts() ) : ?>
                <div class="blocPage">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div>
                        <?php 
                            $img_post = get_the_post_thumbnail_url(get_the_ID());
                            if ($img_post) {
                                $thumb_post = $img_post;
                            }else {
                                $thumb_post = get_template_directory_uri().'/images/default.jpg';
                            }
                        ?>
                        <img src="<?= $thumb_post; ?>" alt="<?= the_title(); ?>"/>
                        <div>
                            <h2 class="sousTitre"><?= the_title(); ?></h2> 
                            <p><?= substr(get_the_excerpt(), 0, 200).' ...'; ?></p>
                            <a href="<?= the_permalink(); ?>" title="Tohiny" class="link">Tohiny</a>
                        </div>
                        <span class="clear"></span>
                    </div>
                    <?php endwhile; ?>
                </div>
            <?php  else : 
                echo "Aucun";
            endif; ?>


            
            
        </div>  
        <?php get_template_part( 'template-parts/content', 'right' ); ?>    
    </main> 
<?php get_footer(); ?>

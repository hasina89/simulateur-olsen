<?php get_header(); ?>

    <?php if ( !is_front_page() ) : ?>

	    <!--News-->
	    <section>
	        <div id="csi-news" class="csi-news">
	            <div class="csi-inner">
	                <div class="container">
	                	<?php if ( have_posts() ) : ?>
	                    <div class="row">
	                    	<?php while ( have_posts() ) : the_post(); ?>
	                        <div class="col-xs-12 col-sm-6 col-md-4">
	                            <div class="csi-single-news">
	                                <figure>
	                                    <a href="<?php the_permalink(); ?>"><img src="<?= the_post_thumbnail_url(); ?>" alt=""></a>	                                    
	                                </figure>
	                                <h3 class="title"><a href="<?php the_permalink(); ?>"><?= the_title(); ?></a></h3>
	                            </div>
	                        </div>
	                        <?php endwhile; ?>


							<div class="text-center">
		                    	<?php 
		                    		the_posts_pagination( array(
                                    'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'cm' ),
                                    'next_text' => __( '<i class="fa fa-angle-right"></i>', 'cm' )
                                    ) ); 
                                ?>
		                    </div>
	                    </div>
						
	                   	<?php endif; ?>
	                </div>
	            </div>
	        </div>
	    </section>	
    <?php endif; ?>
<?php get_footer(); ?>
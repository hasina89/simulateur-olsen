<?php get_header(); ?>

<?php 
global $wp_query;

// get slug author after /
$url = $_SERVER['REQUEST_URI']; 
$values = parse_url($url);
$host = explode('/',$values['path']);
$host = array_filter($host);
$key = array_pop(array_keys($host));
$slug_user = $host[2];


// users
$users = get_users( array( 'fields' => array( 'user_login' ) ));

$utilisateur = array();
foreach ($users as $key => $value) {
    $val = $value->user_login;
    array_push($utilisateur, $val);
}

$slug_user = str_replace('%20', ' ', $slug_user);

$user = get_user_by('login', $slug_user);
if($user){  $user_id = $user->ID;}

$user_data = get_userdata( $user_id );
$author_name = $user_data->display_name;

$user_desc = esc_attr( get_the_author_meta( 'description', $user_id ));

$u_data = get_avatar_data($user_id);

// lien social auteur
$fb_aut = esc_attr( get_the_author_meta( 'facebook', $user_id ));
$tw_aut = esc_attr( get_the_author_meta( 'twitter', $user_id ));
$link_aut = esc_attr( get_the_author_meta( 'linkedin', $user_id ));
$ins_aut = esc_attr( get_the_author_meta( 'instagram', $user_id ));
$pin_aut = esc_attr( get_the_author_meta( 'pinterest', $user_id ));
$you_aut = esc_attr( get_the_author_meta( 'youtube', $user_id ));
$mail = esc_attr( get_the_author_meta( 'user_email', $user_id ));

$url_user = esc_attr( get_the_author_meta( 'url', $user_id ));

// echo $url_user;

$all_post_type = array(
    'post',
    'actualite',
    'enseignement',
    'citation',
    'musique',
    'parole',
    'temoignage',
    'pensee',
    'lifestyle',
    'homme',
    // 'video'
);

// author




// $c_a = count_user_posts($user_id, $all_post_type, true);
// print_r($c_a);
// The Query

$args_auth = array(
  'author'          =>  $user_id, 
  'orderby'         =>  'post_date',
  'order'           =>  'DESC',
  'status'          => 'publish',
  'post_type'       => $all_post_type,
  'posts_per_page'  => 10,
  'paged'           => $paged
);


query_posts( $args_auth );


 ?>

    <div class="banner_title">
        <img src="<?php echo get_template_directory_uri() ; ?>/img/bg_single.png" alt="">
        <h2 class="title_single"><?php echo $author_name; ?></h2>
       
        <div class="bread_crumb">
            <ul>
                <li><a title="Accueil" rel="nofollow" href="<?php echo esc_url( home_url( '/' ) ); ?>">Accueil</a></li>
                <li><span><?php echo $author_name; ?></span></li>
            </ul>
        </div>
    </div>
    <!-- master content -->
    <div id="master_content">
        <div class="inner_master_content">
            <div class="container">
                <div class="row">  
                    <div class="left_content col-sm-8">
                        <div class="inner_right_content row">  
                            <div class="col-xs-12">
                            <?php if (in_array( $slug_user , $utilisateur, true )) : ?>  
                                <h2 class="title_cat subtitle_secondary"><?php echo $author_name; ?></h2>   

                                <div class="all_of_author row no_marg">
                                    <div class="col-xs-12 box_author">
                                        <div class="row">
                                            <div class="col-xs-5 col-sm-4 img_auth">
                                                <?php echo get_avatar( $user_id , 200 ); ?>
                                            </div>
                                            <div class="col-xs-7 col-sm-8 txt_auth">
                                                <h4>A propos de <strong><?php echo ucfirst($author_name); ?></strong></h4>
                                                <p><?php echo $user_desc; ?></p>
                                                <div class="social_single_box">
                                                    <ul>
                                                        <?php if (!empty($fb_aut)) { ?>
                                                            <li><a target="_blank" class="s_facebook" href="<?php echo $fb_aut; ?>" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($tw_aut)) { ?>
                                                        <li><a target="_blank" class="s_twitter" href="<?php echo $tw_aut; ?>" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($link_aut)) { ?>
                                                        <li><a target="_blank" class="s_google" href="<?php echo $link_aut; ?>" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($ins_aut)) { ?>
                                                        <li><a target="_blank" class="s_instagram" href="<?php echo $ins_aut; ?>" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($you_aut)) { ?>
                                                        <li><a target="_blank" class="s_rss" href="<?php echo $you_aut; ?>" title="youtube"><i class="fa fa-youtube"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($pin_aut)) { ?>
                                                        <li><a target="_blank" class="s_pinterest" href="<?php echo $pin_aut; ?>" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($mail)) { ?>
                                                        <li><a target="_blank" class="s_mail" href="mailto:<?php echo $mail; ?>" title="Email"><i class="fa fa-envelope"></i></a></li>
                                                        <?php } ?>
                                                        <?php if (!empty($url_user)) { ?>
                                                            <li><a target="_blank" class="s_url" href="<?php echo $url_user; ?>" title="Site web"><i class="fa fa-link"></i></a></li>
                                                        <?php } ?> 
                                                    </ul>
                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h2 class="subtitle_secondary">Ses articles :</h2>
                     
                                <div class="row all_post_cat">

                                    <?php $Q_aut = new WP_Query( $args_auth ); ?>
                                    <?php if( have_posts()) : ?> 
                                        <?php  while ( have_posts() ) : the_post(); ?>

                                            <?php 
                                                // Category 
                                                $category = get_the_category(); 
                                                $category_id = get_cat_ID( $category[0]->cat_name );
                                                $category_link = get_category_link( $category_id );
                                                // format
                                                $format = get_post_format( $post->ID );  
                                                if (!empty($format)) {
                                                    $format = get_post_format( $post->ID ); 
                                                }else {
                                                    $format = 'quote';
                                                }
                                            ?>
                                            <div class="col-xs-12 col-sm-6 item_post_cat only_cat">
                                                <div class="row no_marg cadre_box">
                                                    <div class="col-xs-5 col-sm-12 img_thumb_box">
                                                        <?php if(has_post_thumbnail()) : ?>
                                                            <?php
                                                                $url_img = get_the_post_thumbnail_url();                  
                                                                $fake_url = site_url().'/wp-includes/images/';
                                                                if (stristr($url_img, $fake_url)) {
                                                                    $ph_img = '';
                                                                }else {
                                                                    $ph_img = $url_img;
                                                                }
                                                            ?>
                                                            <?php if(!empty($ph_img)) : ?>
                                                                <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                            <?php else: ?>
                                                                <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" ></a>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <a class="<?php echo 'at_'.$format; ?>" href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                        <?php endif; ?>
                                                        <a class="cat_sub_img" href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a>

                                                    </div>
                                                    <div class="inner_item_p_c col-xs-7 col-sm-12 ">
                                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                        <?php 
                                                            $extrait = substr(get_the_excerpt(), 0, 200);
                                                            $extrait .= ' ...';
                                                        ?>
                                                        <p><?php echo $extrait; ?></p>                
                                                        <div class="link_to row">
                                                            <div class="left_link_to col-sm-8">
                                                                <a class="author_cat" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_login' ) ); ?>"><?php the_author(); ?></a>
                                                                <span class="date_cat">- <?php $post_date = get_the_date( 'j M, Y' ); echo $post_date; ?></span>
                                                            </div>
                                                            <div class="right_link_to col-sm-4">
                                                                <span class="total_comment_cat"><?php comments_number( '0', '1', '%' ); ?> <i class="fa fa-comments-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        <?php  endwhile; wp_reset_query(); ?>

                                        <div class="page_it col-xs-12">
                                            <?php /*the_posts_pagination( array(
                                            'prev_text'          => __( 'Précédente', 'cm' ),
                                            'next_text'          => __( 'Suivante', 'cm' )
                                            ) );  */
                                        ?></div>
                                        <div class="page_it col-xs-12">
                                            <?php echo ad_pagination($Q_aut->max_num_pages); ?>
                                        </div> 

                                    <?php else : ?>
                                        <div class="col-xs-12">
                                            <p>Aucun article pour cet auteur.</p>
                                        </div>
                                    <?php endif; ?>
                                                                 
                                </div>
                            <?php else : ?>
                                <h2 class="title_cat subtitle_secondary">Identifiant auteur inéxistant ou erreur.</h2>

                            <?php endif; ?>
                            </div>                           
                        </div>
                    </div>
                    <!-- End left -->

                    <!-- Sidebar right -->
                    <?php get_template_part( 'template-parts/content', 'sidebar-right' ); ?>
                    
                </div>
            </div>          
        </div>  


<?php get_footer(); ?>
<?php 

/* SIDEBAR */
function ad_widget_init() {	
	// LATERAL
    register_sidebar( array(
        'name'          => esc_html( __( 'Sidebar right', 'ad' ) ),
        'id'            => 'sidebar_right',
        'description'   => esc_html( __( 'Sidebar right', 'ad' ) ),
        'before_widget' => '<div class="sidebar-widget sidebar-category">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="sidbar-title"><h4>',
        'after_title'   => '</h4></div>'
    ) ); 

    register_sidebar( array(
        'name'          => esc_html( __( 'Sidebar Bottom', 'ad' ) ),
        'id'            => 'sidebar_bottom',
        'description'   => esc_html( __( 'Sidebar Bottom', 'ad' ) ),
        'before_widget' => '<div class="sidebar-widget sidebar-category">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="sidbar-title">',
        'after_title'   => '</div>'
    ) ); 
}
add_action( 'widgets_init', 'ad_widget_init' );
?>
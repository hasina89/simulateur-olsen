<?php 
    function catch_first_img() {
        global $post, $posts;
        $first_img = '';
        ob_start();
        ob_end_clean();
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

        if (count($output) > 0) {
            if (!empty($matches)) {
                if (isset($matches[1])) {
                    if ( isset($matches [1] [0])) {
                        $first_img = $matches [1] [0];
                    }                    
                }
            }else {
                $first_img = '';
            }                   
        }  

        if(!empty($first_img)){ 
            if (count($first_img) > 0) {
                return $first_img;  
            }                      
        }else {
            $first_img = get_template_directory_uri() ."/img/default.png"; 
        }      
        
        return $first_img;
    } 
?>
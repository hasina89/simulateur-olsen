<?php
	// function pluralize( $count, $text ) 
	// { 
	//     return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
	// }

	// function ago( $datetime )
	// {
	//     $interval = date_create('now')->diff( $datetime );
	//     $suffix = ( $interval->invert ? ' ago' : '' );
	//     if ( $v = $interval->y >= 1 ) return pluralize( $interval->y, 'year' ) . $suffix;
	//     if ( $v = $interval->m >= 1 ) return pluralize( $interval->m, 'month' ) . $suffix;
	//     if ( $v = $interval->d >= 1 ) return pluralize( $interval->d, 'day' ) . $suffix;
	//     if ( $v = $interval->h >= 1 ) return pluralize( $interval->h, 'hour' ) . $suffix;
	//     if ( $v = $interval->i >= 1 ) return pluralize( $interval->i, 'minute' ) . $suffix;
	//     return pluralize( $interval->s, 'second' ) . $suffix;
	// }

function get_timespan_string($older, $newer) { 
  $Y1 = $older->format('Y'); 
  $Y2 = $newer->format('Y'); 
  $Y = $Y2 - $Y1; 

  $m1 = $older->format('m'); 
  $m2 = $newer->format('m'); 
  $m = $m2 - $m1; 

  $d1 = $older->format('d'); 
  $d2 = $newer->format('d'); 
  $d = $d2 - $d1; 

  $H1 = $older->format('H'); 
  $H2 = $newer->format('H'); 
  $H = $H2 - $H1; 

  $i1 = $older->format('i'); 
  $i2 = $newer->format('i'); 
  $i = $i2 - $i1; 

  $s1 = $older->format('s'); 
  $s2 = $newer->format('s'); 
  $s = $s2 - $s1; 

  if($s < 0) { 
    $i = $i -1; 
    $s = $s + 60; 
  } 
  if($i < 0) { 
    $H = $H - 1; 
    $i = $i + 60; 
  } 
  if($H < 0) { 
    $d = $d - 1; 
    $H = $H + 24; 
  } 
  if($d < 0) { 
    $m = $m - 1; 
    $d = $d + get_days_for_previous_month($m2, $Y2); 
  } 
  if($m < 0) { 
    $Y = $Y - 1; 
    $m = $m + 12; 
  } 
  $timespan_string = create_timespan_string($Y, $m, $d, $H, $i, $s); 
  return $timespan_string; 
} 

?>


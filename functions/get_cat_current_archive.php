<?php
// get_post_type
global $wp;
$current_url = get_home_url(null, $wp->request, null);
$queried_object = get_queried_object();
$post_type = $queried_object->name;

$exclusive_post_type = $post_type;
$exclude_cats        = array();
$nonemptycats        = get_terms( 'category', array( 'hide_empty' => false, 'hide_if_empty'      => false) );
foreach( $nonemptycats as $term ){
  $term_id      = $term->term_id;
  $term_objects = get_objects_in_term( $term_id, 'category' );
  $exclude_term = true;
  foreach( $term_objects as $post_id ){
    if ( $exclusive_post_type === get_post_type( $post_id ) ){
      $exclude_term = false;
      break;
    }
  }
  if ( $exclude_term ) $exclude_cats[] = $term_id;
}

// category
$args_cat = array(
    'show_option_all'    => '',
    'show_option_none'   => 'Choisir',
    'option_none_value'  => 'none',
    'orderby'            => 'ID',
    'order'              => 'ASC',
    'show_count'         => 0,
    'hide_empty'         => 0,
    'child_of'           => 0,
    'include'            => '',
    'echo'               => 1,
    'selected'           => 0,
    'hierarchical'       => 0,
    'name'               => 'categ',
    'id'                 => 'cat',
    'class'              => 'form-control selectpicker show-tick',
    'depth'              => 0,
    'tab_index'          => 0,
    'taxonomy'           => 'category',
    'hide_if_empty'      => false,
    'value_field'        => 'slug',
    'exclude'         => $exclude_cats,
);

if (isset($_GET['categ'])) {
    $args_cat['selected'] = $_GET['categ'];
}


function attrib_perso_select($output){
  return str_replace('<select','<select data-live-search="true" data-size="5"',$output);
}
add_filter('wp_dropdown_cats','attrib_perso_select');


?>
<?php 


// title (page, post, archive ...)
function somaco_title( $bEcho = true ) {
    global $post;
    $store_type_get = somaco_postGetValue( "type", "" );
    $header_type = get_term_by( 'slug', $store_type_get, 'store_type' );
    $DD_meta_title = get_post_meta( $post->ID, '_cmb_dreamdare_meta_title', true );
    $title = ( !empty( $DD_meta_title ) ) ? $DD_meta_title : "";
    global $paged ;
    if (empty( $title ) ) {
        if ( is_archive() ) {
            $title = post_type_archive_title( '', false );
        }
        if ( is_page() ){
            $title = the_title( '', '', false );
        }
        if ( is_single() ) {
            $title = single_post_title( '', false );
        }
        if (!empty ( $header_type ) ) {
            $title .= ' ' . $header_type->name;
        }
        $title .= ' : ' . get_bloginfo( 'name' );
    }
    $title = ( !empty( $title ) ) ? $title :  __( 'Contenu introuvable !', 'somaco' )." : Adore Dieu";
    echo $title;
}



// COMMON
function somaco_get_theme_mod_image( $name = "", $size = "thumbanail", $default = null )
{
    global $_wp_additional_image_sizes;
    $image = get_theme_mod ($name, $default) ;
    if ( !empty( $image ) && isset( $_wp_additional_image_sizes[$size] ) ) {
        $paths = pathinfo ($image) ;
        $sizes = $_wp_additional_image_sizes[$size];
        $wp_upload_dir = wp_upload_dir();
        $basedir = $wp_upload_dir["basedir"];
        $dirname = $paths["dirname"];
        $dirnames = explode( 'wp-content/uploads', $dirname );
        $dirdate = ( isset( $dirnames[1] ) ) ? $dirnames[1] : "";
        $templatepath = $basedir . $dirdate . '/' ;

        if ( (isset( $paths["dirname"] ) ) && ( isset( $paths["filename"] ) ) && ( isset( $paths["extension"] ) ) && ( !empty( $size ) ) ) {

            if (is_file ($templatepath . $paths ["filename"] . "-" . $sizes["width"] . "x" . $sizes["height"] . "." . $paths ["extension"])) {

                $image = $paths ["dirname"] . "/" . $paths ["filename"] . "-" . $sizes["width"] . "x" . $sizes["height"] . "." . $paths ["extension"];

            } else {
                $image = $paths ["dirname"] . "/" . $paths ["filename"] . "." . $paths ["extension"];
            }
        }
        return $image ;
    }
}

// common
function somaco_postGetValue( $name = "", $default_value = null ) {
    $res = $default_value ;
    if ( isset( $_GET[$name] ) ) {
        $res = $_GET[$name];
    } elseif (isset($_POST[$name])) {
        $res = $_POST[$name];
    }
    return $res;
}
// Current post type
function somaco_get_current_post_type() {
    global $post, $typenow, $current_screen ;

    if ( $post && $post->post_type ) {
        return $post->post_type;
    } elseif ( $typenow ) {
        return $typenow;
    } elseif ( $current_screen && $current_screen->post_type ) {
        return $current_screen->post_type;
    } elseif ( isset( $_REQUEST['post_type'] ) ) {
        return sanitize_key( $_REQUEST['post_type'] );
    } elseif( isset( $_GET['post'] ) ) {
        $thispost = get_post( $_GET['post'] );
        return $thispost->post_type;
    } else {
        return null;
    }

}

// Troncature d'une chaîne
function ad_truncate( $inputStr = "", $maxLen = 385, $lastCont = " . . . " ) {
    $res = strip_tags( $inputStr );
    if ( strlen( $res) > $maxLen ) {
        $res = mb_substr( strip_tags( $inputStr ), 0, $maxLen, "UTF-8" );
        $res .= $lastCont;
    }
    return $res ;
}
// Make uppercase first char of a given word
function mb_ucfirst( $inputStr, $encoding = 'UTF-8' ) {
    $res = $inputStr;
    if ( !is_null( $inputStr ) && ( $inputStr != "" ) ) {
        $res = mb_strtoupper( mb_substr( $inputStr, 0, 1, $encoding ), $encoding ) . mb_substr( $inputStr, 1, mb_strlen( $inputStr, $encoding ) - 1, $encoding );
    } 
    return $res;
}



// date to string
function ad_dateToStrFr( $date = "", $format = "A d B Y" ) {
    $date = str_replace( "/", "-", $date );
    $date = new DateTime( $date );
    return $date->format( $format );
}


// get image source

function somaco_get_image_src( $image_id = null, $imagesize = 'thumbnail' ) {
    if ( !empty( $image_id ) ) {
        $post_thumbnail_img = wp_get_attachment_image_src( $image_id, $imagesize );
        return ( isset( $post_thumbnail_img[0] ) ) ? $post_thumbnail_img[0] : null;
    }
}




// image feature
function somaco_get_featured_image( $post_ID = null, $imagesize = 'thumbnail' ) {

    $post_thumbnail_id = get_post_thumbnail_id( $post_ID );

    if ( !empty( $post_thumbnail_id ) ) {

        $post_thumbnail_img = ( !empty( $imagesize ) ) ? wp_get_attachment_image_src( $post_thumbnail_id, $imagesize ) : wp_get_attachment_image_src( $post_thumbnail_id );

        return (isset ( $post_thumbnail_img[0] ) ) ? $post_thumbnail_img[0] : null;

    }

}


// featured image infos
function somaco_get_featured_image_infos( $post_ID = 0 ) {

    $informations = new stdClass();

    $attachment = get_post( get_post_thumbnail_id ($post_ID ) );

    if (!empty( $attachment ) ) {

        $informations->title = $attachment->post_title;

        $informations->alt = get_post_meta( get_post_thumbnail_id( $post_ID ), '_wp_attachment_image_alt', true );

        $informations->description = $attachment->post_content;

        $informations->legend = $attachment->post_excerpt;

    }

    return $informations ;

}

 ?>
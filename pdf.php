<?php
// Template Name: Page PDF
require_once(xoousers_path.'tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('test');
$pdf->SetAuthor('Adore Dieu');
$pdf->SetTitle('Votre facture');
$pdf->SetSubject('Mois de');
$pdf->SetKeywords('ABC, PDF, example, test, guide');

// echo PDF_HEADER_STRING; die();

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(11,176,213), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// remove default header
// $pdf->setPrintHeader(false);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 11, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set bacground image
$img_file = K_PATH_IMAGES.'bg01.png';
$pdf->Image($img_file, 0, 0, 210, 0, 'PNG', '', '', false, 300, '', false, false, 0);
// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

/**
 * DATA
 */

$cur_user_id = $_GET['user'];
$fact_id = $_GET['fact_id'];

// check post
$fact_args = array (
    'post_type'         => 'facture',
    'post_status'       => 'publish',
    'p'                 => $fact_id   
);

$fact = get_posts($fact_args);
$post_id = $fact[0]->ID;

// content
$post_content = $fact[0]->post_content;

// print_r($post_content); die();

// Paiement OK ou KO
$user_paed_type = get_post_meta($post_id, 'user_paed_type',true);
/**
 * Message
 */
$msg_fact_paye = get_post_meta($post_id, 'msg_fact_paye',true);
$msg_fact_non_paye = get_post_meta($post_id, 'msg_fact_non_paye',true);

switch ($user_paed_type) {
    case 'Aucun':
        $user_have_paied = 'Pas encore payé';
        $notification = $msg_fact_non_paye;
        break;

    case 'Seulement':
        $user_payed = get_post_meta($post_id, 'user_payed',true); 
        if (in_array($cur_user_id, $user_payed)) {
            $user_have_paied = 'Déjà payé';
            $notification = $msg_fact_paye;
        }else {
            $user_have_paied = 'Pas encore payé';
            $notification = $msg_fact_non_paye;
        }
        break;

    case 'Tous sauf':
        $user_not_payed = get_post_meta($post_id, 'user_not_payed',true);
        if (in_array($cur_user_id, $user_not_payed)) {
            $user_have_paied = 'Pas encore payé';
            $notification = $msg_fact_non_paye;
        }else {
            $user_have_paied = 'Déjà payé';
            $notification = $msg_fact_paye;
        }        
        break;

    case 'Tous':
        $user_have_paied = 'Déjà payé';
        $notification = $msg_fact_paye;
        break;
 } 

/**
 * Table
 */

$table = ' 
<!-- EXAMPLE OF CSS STYLE -->
<style>
    p { margin-bottom: 10px; }
    table.blueTable {
      border: 1px solid #1C6EA4;
      background-color: #EEEEEE;
      width: 100%;
      text-align: center;
      border-collapse: collapse;
      vertical-align: middle;
    }
    table.blueTable tr td, table.blueTable th {
      border: 1px solid #AAAAAA;
      font-size: 13px;
      line-height: 30px;
    }    
    table.blueTable tr th {
      font-size: 15px;
      line-height: 30px;
      font-weight: bold;
      color: #FFFFFF;
      text-align: center;
      background-color: #0bb0d5;
    }    

    table.ownertable { float: right; border: none; background-color: #efefef; width: 100%; }
    table.ownertable tr td  { line-height: 25px; font-size: 12px; border-bottom: 1px dashed #aaaaaa; }

</style>';

$table .= '
<div> 
    <table width="100%">
        <tr>
            <td width="45%">&nbsp;</td>
            <td width="55%">  
                <table class="ownertable">
                    <tr><td>&nbsp;<strong>Identifiant :</strong> 12345</td></tr>
                    <tr><td>&nbsp;<strong>Nom :</strong> Votre nom</td></tr>
                    <tr><td>&nbsp;<strong>Adresse :</strong> Votre adresse</td></tr>
                    <tr><td>&nbsp;<strong>Status :</strong> Partenaire Simple</td></tr>
                </table>
            </td>
        </tr>
    </table>';


$table .= $post_content . '<p></p>';

$table .= '<h4><em>Remarque au destinatiare :</em></h4>';
$table .= '<em style="background-color: #eee;">'. $notification . '</em>';
$table .= '<p></p>';

$table .= '
    <table class="blueTable">
        <tr>
            <th>Description</th>
            <th>Mois</th>
            <th>Montant</th>
            <th>Etat</th>
        </tr>
    ';

$table .="<tr>";

$Q_fact = new WP_Query( $fact_args ); 
if ( $Q_fact->have_posts() ) : while ( $Q_fact->have_posts() ) : $Q_fact->the_post();

    $table .= '<td align="center">'. get_field('intitule_facture') .'</td>';
    $table .= '<td align="center">'. get_field('date_facture') .'</td>';
    $table .= '<td align="center">'. get_field('montant_facture') .'</td>';
    

endwhile; wp_reset_postdata(); endif;

$table .= '<td align="center"><strong>' . $user_have_paied . '</strong></td>';

$table .="</tr>";

$table .="<tr>";
    $table .= '<td align="center"  colspan="4">&nbsp;</td>';
$table .="</tr>";

$table .="<tr>";
    $table .= '<td align="right"  colspan="3">Total à payer</td>';
    $table .= '<td align="center"><strong>'. get_post_meta($post_id, 'montant_facture',true) .' €</strong></td>';
$table .="</tr>";

$table .="</table>";

$table .= '<p>&nbsp;</p>';  
$img_file = xoousers_url.'tcpdf/examples/images/signature.png';

// $signature = get_post_meta($post_id, 'signature',true);

// print_r($signature); die();

$table .= '
    <p style="text-align: right;">L\'Equipe d\'AdoreDieu.com</p>
    <p style="text-align: right;"><img class="alignnone " src="'.$img_file.'" alt="" width="223" height="56">';

// print_r($table);
// die();

// Set some content to print
$html = <<<EOF
    $table
EOF;

$current_user = wp_get_current_user();
$user_id = $current_user->id;



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// *** set an empty signature appearance ***
$pdf->addEmptySignatureAppearance(180, 80, 15, 15);


if ( $user_id == $cur_user_id ) {
	// Print text using writeHTMLCell()
	// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    $pdf->writeHTML($html, true, false, true, false, '');
    
}
else {
	wp_die('Ne Trichez pas SVP !!!');
}

/* ------------------------------- */


$fact_name_sanitize = preg_replace('/[^A-Za-z0-9\. -]/', '', $fact[0]->post_name);
$fact_name_sanitize = preg_replace('/  */', '-', $fact_name_sanitize);
$fact_name_sanitize = preg_replace('/\\s+/', '-', $fact_name_sanitize);

$fact_name = $fact_name_sanitize .'-'. $current_user->user_login;


$pdf->Output($fact_name.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

